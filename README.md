# subtitle.sh

> Bash shell skripta za automatsku zamenu karaktera ð, æ, Æ, è, È slovima đ, ć, Ć, č, Č u subtitle fajlovima.

## Korišćenje

Skripta 'subtitle.sh' se može koristiti na više načina:

- da izvrši automatsku zamenu karaktera u jednom zadatom fajlu
- da izvrši automatsku zamenu karaktera u svim fajlovima u zadatom folderu
- da izvrši automatsku zamenu karaktera u svim fajlovima u zadatom folderu i svim subfolderima

**Ukoliko se skripti prosledi samo ime foldera zamena će se izvršiti u svim pronađenim fajlovima koji imaju ekstenziju '.sub' ili '.srt'.**
**Ukoliko subtitle fajl ima neku drugu ekstenziju moguće je proslediti skripti tu ekstenziju kao argument nakon imena foldera i zamena karaktera će se izvršiti samo u tim fajlovima. Pogledati primere 4 i 5 ispod.**
Skriptu je naravno moguće koristiti i za bilo koji drugi tekstualni fajl u kome je potrebno zameniti karaktere ð, æ, Æ, è, È.

#### Primeri

``` bash

1. Izvrši zamenu u samo jednom fajlu (fajl može da ima bilo koju subtitle ekstenziju, .sub, .srt ...):"
  $ ./subtitle.sh '/putanja_do_titla/ime_titla.srt'"

2. Izvrši zamenu u svim fajlovima (.srt, .sub) u nekom folderu:"
  $ ./subtitle.sh '/putanja_do_foldera/ime_foldera'"

3. Izvrši zamenu u svim fajlovima (.srt, .sub) u nekom folderu i svim subfolderima:"
  $ ./subtitle.sh -r '/putanja_do_foldera/ime_foldera'"

4. Izvrši zamenu u svim fajlovima sa sledećom ekstenzijom (u primeru .srt i .gsub) u nekom folderu:"
  $ ./subtitle.sh '/putanja_do_foldera/ime_foldera' srt gsub"

5. Izvrši zamenu u svim fajlovima sa sledećom ekstenzijom (u primeru .srt i .gsub) u nekom folderu i svim subfolderima:"
  $ ./subtitle.sh -r '/putanja_do_foldera/ime_foldera' srt gsub"

```

### Opcije

Skripta može da primi pet opcija ali čak četiri od njih vode ka istom rezultatu, ispisuju kratko uputstvo kako koristiti skriptu:

``` bash

Rekurzivno zameni karaktere u svim fajlovima koji se nalaze u datom folderu i svim subfolderima:
  -r

Za kratko uputsvo kako koristiti skriptu iskoristite bilo koju od sledeće četiri opcije:
  --info, -i, --help, -h

Primer:
  $ ./subtitle.sh --help

```

## Odricanje od odgovornosti

Ova skripta je moj mali projekat testiran samo na mom računaru. Ne garantujem da će raditi na vašem računaru i ne snosim nikakvu odgovornost ukoliko dođe do bilo kakvih neželjenih posledica njenim korišćenjem. Koristiti sa oprezom i pratiti uputstvo.

Skriptu je dozvoljeno koristiti, menjati tako da odgovara vašim potrebama, i deliti.

Ukoliko imate pitanja, predloge ili kritike slobodno me kontaktirajte.
