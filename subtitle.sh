#!/bin/bash

info() {
  echo "######################################################################################################################"
  echo "Ova skripta omogućava automatsku zamenu karaktera ð, æ, Æ, è, È slovima đ, ć, Ć, č, Č."
  echo 
  echo "- Izvrši zamenu u samo jednom fajlu (fajl može da ima bilo koju subtitle ekstenziju, .sub, .srt ...):"
  echo "    ./subtitle.sh '/putanja_do_titla/ime_titla.srt'"
  echo
  echo "- Izvrši zamenu u svim fajlovima (.srt, .sub) u nekom folderu:"
  echo "    ./subtitle.sh '/putanja_do_foldera/ime_foldera'"
  echo
  echo "- Izvrši zamenu u svim fajlovima (.srt, .sub) u nekom folderu i svim subfolderima:"
  echo "    ./subtitle.sh -r '/putanja_do_foldera/ime_foldera'"
  echo
  echo "- Izvrši zamenu u svim fajlovima sa sledećom ekstenzijom (u primeru .srt i .gsub) u nekom folderu:"
  echo "    ./subtitle.sh '/putanja_do_foldera/ime_foldera' .srt .gsub"
  echo
  echo "- Izvrši zamenu u svim fajlovima sa sledećom ekstenzijom (u primeru .srt i .gsub) u nekom folderu i svim subfolderima:"
  echo "    ./subtitle.sh -r '/putanja_do_foldera/ime_foldera' .srt .gsub"
  echo "######################################################################################################################"
}

# File's encoding has to be changed in order for character substitution with 'sed' to work
# Files that have characters ð, æ, Æ, è, È in them have 'unknown-8bit' encoding which can't be used with 'iconv'
# However, we know that those files are encoded in 'windows-1252' so we use that with 'iconv'
changeEncoding() {
  encodingOld=$(file -b --mime-encoding "$1")
  if [ $encodingOld = "unknown-8bit" ]
    then
    iconv -f windows-1252 -t utf8 "$1" > temp.txt && mv temp.txt "$1"
  elif [ $encodingOld != "utf-8" ]
    then
    iconv -f $encodingOld -t utf8 "$1" > temp.txt && mv temp.txt "$1"
  fi
}

if [ $# -eq 0 ]
  then
  info
elif [ $# -eq 1 ]
  then
  if [ -f "$1" ]
    # FILE AS ONLY ARGUMENT
    then
    changeEncoding "$1"
    sed -i -e 's/ð/đ/g' "$1"
    sed -i -e 's/æ/ć/g' "$1"
    sed -i -e 's/Æ/Ć/g' "$1"
    sed -i -e 's/è/č/g' "$1"
    sed -i -e 's/È/Č/g' "$1"
  elif [ -d "$1" ]
    # FOLDER AS ONLY ARGUMENT
    then
    find "$1" -maxdepth 1 -type f \( -name "*.srt" -o -name "*.sub" \) -print0 | while read -d $'\0' file
      do
          changeEncoding "$file"
      done
    find "$1" -maxdepth 1 -type f \( -name "*.srt" -o -name "*.sub" \) -execdir sed -i -e 's/ð/đ/g;s/æ/ć/g;s/Æ/Ć/g;s/è/č/g;s/È/Č/g' {} \;
  elif [ "$1" = "--help" -o "$1" = "-h" -o "$1" = "--info" -o "$1" = "-i" ]
    # --help AS ONLY ARGUMENT
    then
    info
  else
  echo "Greška! '$1' ne postoji! Proverite da li ste dobro uneli putanju fajla/foldera!"
  echo "Za više informacija pokrenite './subtitle.sh --help'."
  fi
else
  if [ "$1" = "-r" ]
    then
    if [ -d "$2" ]
      then
      # FOLDER/SUBFOLDER
      if [ $# -eq 2 ]
        then
        find "$2" -type f \( -name "*.srt" -o -name "*.sub" \) -print0 | while read -d $'\0' file
          do
              changeEncoding "$file"
          done
        find "$2" -type f \( -name "*.srt" -o -name "*.sub" \) -execdir sed -i -e 's/ð/đ/g;s/æ/ć/g;s/Æ/Ć/g;s/è/č/g;s/È/Č/g' {} \;
      else
      # FOLDER/SUBFOLDER + ext
      for var in "${@:3}" # skipping first two arguments
        do
          find "$2" -type f -name "*.$var" -print0 | while read -d $'\0' file
            do
                changeEncoding "$file"
            done
          find "$2" -type f -name "*.$var" -execdir sed -i -e 's/ð/đ/g;s/æ/ć/g;s/Æ/Ć/g;s/è/č/g;s/È/Č/g' {} \;
        done
      fi
    else
      echo "Greška! Folder '$2' ne postoji! Proverite da li ste dobro uneli putanju foldera!"
      echo "Za više informacija pokrenite './subtitle.sh --help'."
    fi
  else
  # FOLDER + ext
  for var in "${@:2}" # skipping first argument
    do      
      find "$1" -maxdepth 1 -type f -name "*.$var" -print0 | while read -d $'\0' file
        do
            changeEncoding $file
        done
      find "$1" -maxdepth 1 -type f -name "*.$var" -execdir sed -i -e 's/ð/đ/g;s/æ/ć/g;s/Æ/Ć/g;s/è/č/g;s/È/Č/g' {} \;
    done
  fi
fi
